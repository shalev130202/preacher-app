﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using preacher.Interface;
using Firebase.Storage;
using System.Threading.Tasks;
using preacher.Droid.Interface;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidFirebaseStorage))]
namespace preacher.Droid.Interface
{

    class AndroidFirebaseStorage:IFirebaseStorage
    {
        FirebaseStorage firebaseStorage = new FirebaseStorage("preacher-app-databse.appspot.com");

        public async Task<string> UploadFile(Stream fileStream, string fileName)
        {
            if (firebaseStorage == null)
            {
                return "error firebaseStorage";
            }
            if (fileStream == null)
            {
                return "error fileStream";
            }
            var imageUrl = await firebaseStorage
                .Child("PrifilePic")
                .Child(fileName)
                .PutAsync(fileStream);
            
            return imageUrl;
        }

        public async Task<string> GetFile(string fileName)
        {
            return await firebaseStorage
                .Child("PrifilePic")
                .Child(fileName)
                .GetDownloadUrlAsync();
        }
        public async Task DeleteFile(string fileName)
        {
            await firebaseStorage
                 .Child("PrifilePic")
                 .Child(fileName)
                 .DeleteAsync();

        }
    }
}