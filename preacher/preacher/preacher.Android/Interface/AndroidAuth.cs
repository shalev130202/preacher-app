﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using preacher.Interface;
using Firebase.Auth;
using Xamarin.Forms;
using preacher.Droid.Interface;
using Firebase.Database;
using Firebase.Storage;

[assembly: Dependency(typeof(AndroidAuth))]
namespace preacher.Droid.Interface
{

    class AndroidAuth : IFirebase
    {
        
        ProgressDialog progressDialog;
        FirebaseClient client;

        private Android.Net.Uri filePath;
        private Activity prev;
        public AndroidAuth()
        {

           
        }
        public AndroidAuth(Activity prev)
        {
            
        }

        public async void DeleteUser(string e,string p)
        {
            await FirebaseAuth.Instance.SignInWithEmailAndPasswordAsync(e,p);
            
            FirebaseUser user = FirebaseAuth.Instance.CurrentUser;
            await user.DeleteAsync();
        }
            public async Task<string> DoLoginWithEP(string E, string P)
        {
            try
            {
                var user = await FirebaseAuth.Instance.SignInWithEmailAndPasswordAsync(E, P);

                var token = await user.User.GetIdTokenAsync(false);
                

            return token.Token;
            }
            catch (FirebaseAuthInvalidUserException not_found)
            {
                return "not_found";
            }
            catch (FirebaseAuthInvalidCredentialsException not_match)
            {
                return ("not_match");
            }
            catch (System.Exception e)
            {

                return e.Message;
            }
            
        }

        public async Task<string> DoRegisterWithEP(string E, string P)
        {
            try
            {
                var user = await FirebaseAuth.Instance.CreateUserWithEmailAndPasswordAsync(E, P);
     

                var token = await user.User.GetIdTokenAsync(false);
                
                return token.Token;
            }
            catch (FirebaseAuthWeakPasswordException weakPassword)
            {
                return "weakPassword";
            }
            catch (FirebaseAuthUserCollisionException user_already_exists)
            {
                return "user_already_exists";
            }
            catch (FirebaseAuthInvalidCredentialsException invaildEmail)
            {
                return "invaildEmail";
            }
            
            catch (System.Exception e)
            {
                return "unknow error";
            }
        }

        public async void ResetPassword(string E)
        {

            await FirebaseAuth.Instance.SendPasswordResetEmailAsync(E);

        }
    }
}