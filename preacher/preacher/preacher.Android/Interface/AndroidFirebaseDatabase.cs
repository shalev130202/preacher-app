﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Database;
using preacher.Model;
using Firebase.Database.Query;
using preacher.Droid.Interface;
using Xamarin.Forms;
using preacher.Interface;

[assembly: Dependency(typeof(AndroidFirebaseDatabase))]
namespace preacher.Droid.Interface
{
    public class AndroidFirebaseDatabase: IFirebaseDatabase
    {
        FirebaseClient firebase = new FirebaseClient("https://preacher-app-databse.firebaseio.com");
        public async Task AddUser(User user)
        {
            

            await firebase
              .Child("Users").Child(user.UserName)
              .PostAsync(new User() { UserName = user.UserName, BirthDate = user.BirthDate, category= user.category, Information= user.Information,ProfileImage= user.ProfileImage,Token=user.Token});
        }

    }
}