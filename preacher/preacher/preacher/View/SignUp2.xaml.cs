﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using preacher.Interface;
using preacher.View;
using System.IO;
using preacher.Model;
namespace preacher.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUp2 : ContentPage
    {
        Stream file;
        bool uploadflag = false;
        User user;
        string Pass;
        string Email;
        public SignUp2(string Token,string email, string pass )
        {
            Stream file;
            user = new User();
            user.Token = Token;
            this.Pass = pass;
            this.Email = email;
            InitializeComponent();
            backLayout.GestureRecognizers.Add(new TapGestureRecognizer((view) => backToLogin()));
        }
        private async void backToLogin()
        {
            var fbstorag = DependencyService.Get<IFirebaseStorage>();
            var fbauth = DependencyService.Get<IFirebase>();
            fbauth.DeleteUser(Pass,Email);
            App.Current.MainPage = new MainPage("", "");
            if (user.ProfileImage != null)
            {
                fbstorag.DeleteFile(user.Token);
            }
        }
        private async void btnProfileImg_Clicked(object sender, EventArgs e)
        {
        
            try
            {

 
                FileData fileData = await CrossFilePicker.Current.PickFile();
                if (fileData == null)
                    return; // user canceled file picking

                string fileName = fileData.FileName;
                string contents = System.Text.Encoding.UTF8.GetString(fileData.DataArray);
                file = fileData.GetStream();
               
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Exception choosing file: " + ex.ToString());
            }
        }

        private async void btnSignUp_Clicked(object sender, EventArgs e)
        {
            var fbstorag = DependencyService.Get<IFirebaseStorage>();
            var fbdatabase = DependencyService.Get<IFirebaseDatabase>();
            user.Information = information.Text;
            user.UserName = userName.Text;
            try
            {
                user.category = category.SelectedItem.ToString();
            }
            catch (Exception ef)
            {
                DisplayAlert("SingUp", "please choose category", "Ok");
                return;
            }
            user.BirthDate = birthDate.Date.ToString();
            
          
            if (user.BirthDate == System.DateTime.Today.ToString())
            {
                DisplayAlert("SingUp", "please fill out your birth date", "Ok");
            }
            else if (user.UserName ==null)
            {
                DisplayAlert("SingUp", "please fill out user name", "Ok");
            }
            else if (user.Information =="")
            {
                DisplayAlert("SingUp", "please write a few sentences about yourself", "Ok");
            }
            else 
            {
                user.ProfileImage = await fbstorag.UploadFile(file, user.UserName);
                if (user.ProfileImage == null)
                {
                    DisplayAlert("SingUp", "please uplaod profileimage", "Ok");
                    return;
                }
                fbdatabase.AddUser(user);
                //to do: insert user to database
                DisplayAlert("SingUp", "signUp complite Sucessfuly!", "Ok");
            }


        }

       
    }
}