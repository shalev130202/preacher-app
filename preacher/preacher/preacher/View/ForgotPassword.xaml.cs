﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using preacher.Interface;
using preacher.View;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace preacher.View
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    [DesignTimeVisible(false)]
    public partial class ForgotPassword : ContentPage
    {
        public ForgotPassword(string email)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            EntryForgotPass.Text = email;
            SizeChanged += ForgotPassword_SizeChanged;
            backLayot.GestureRecognizers.Add(new TapGestureRecognizer((view) => returntologin()));

        }
        void returntologin()
        {
            
                App.Current.MainPage = new NavigationPage(new MainPage(EntryForgotPass.Text,""));
            
        }
        private void ForgotPassword_SizeChanged(object sender, EventArgs e)
        {

            if (this.Width > this.Height)
            {
                locklogo.HeightRequest = 75;
                locklogo.WidthRequest = 75;
            }
            else
            {
                locklogo.HeightRequest = 120;
                locklogo.WidthRequest = 120;
            }
        }

        private async void ForgotPasswordBTN_Clicked(object sender, EventArgs e)
        {
            string mail = EntryForgotPass.Text;
            

            var fblogin = DependencyService.Get<IFirebase>();

            fblogin.ResetPassword(mail); ;
            await DisplayAlert("email link sent", "we sent an email to "+mail+" with link to reset your password", "Ok");
        }
    }
}