﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using preacher.Interface;
using preacher.View;
using System.IO;

namespace preacher.View
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    [assembly:Dependency(typeof(IFirebaseStorage))]
    [assembly: Dependency(typeof(IFirebase))]
    public partial class SignUp : ContentPage
    {

        public SignUp()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            SizeChanged += MainPage_SizeChanged;
            
            InitializeComponent();
            backLayout.GestureRecognizers.Add(new TapGestureRecognizer((view) => backToLogin()));

            var vPasswordEntry = new Entry() { IsPassword = true };


        }
        private void backToLogin()
        {
            App.Current.MainPage = new MainPage("","");
        }
        private void MainPage_SizeChanged(object sender, EventArgs e)
        {
            if (this.Width > this.Height)
            {
                Xamarin.Forms.Thickness x;
                x = new Xamarin.Forms.Thickness(0.0, 10.0, 0.0, 0.0);
                backLayout.Margin = x;
                x = new Xamarin.Forms.Thickness(175.0, 0.0, 175.0, 0.0);
                btnContinue.Margin = x;
                btnContinue.HeightRequest = 40;
                btnContinue.FontSize = 12;
                logo.HeightRequest = Width * 0.15;
            }
            else
            {
                Xamarin.Forms.Thickness x;
                x = new Xamarin.Forms.Thickness(0.0, 15.0, 0.0, 0.0);
                backLayout.Margin = x;
                x = new Xamarin.Forms.Thickness(10.0, 0.0, 10.0, 0.0);
                btnContinue.Margin = x;
                btnContinue.HeightRequest = 50;
                btnContinue.FontSize = 14;

                logo.HeightRequest = Width * 0.4;
            }


        }
        void backtologin()
        {
            App.Current.MainPage = new MainPage(entryEmail.Text,entryPassword.Text);
        }
        
  
        private async void btnContinue_Clicked(object sender, EventArgs e)
        {
            bool flag = true;
            if (entryPassword.Text == "" || entryPassword.Text == null|| entryEmail.Text == "" || entryEmail.Text == null)
            {
                DisplayAlert("Register", "Please enter email and password .", "Ok");
            }
            if (entryPassword.Text != entryrePassword.Text)
            {
                DisplayAlert("Register", "The password and the Re-password do not mach.", "Ok");
                flag = false;
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(entryEmail.Text);

            }
            catch
            {
                flag = false;
                DisplayAlert("Register", "The Email adress format is incorrect.", "Ok");
            }
            if (flag)
            {
                string mail = entryEmail.Text;
                string pass = entryPassword.Text;

                var fblogin = DependencyService.Get<IFirebase>();

                string returnMsg = await fblogin.DoRegisterWithEP(mail, pass);
                if (returnMsg == "weakPassword")
                {
                    await DisplayAlert("SingIn", "your password is too weak", "Ok");
                }
                else if (returnMsg == "user_already_exists")
                {
                    await DisplayAlert("SingIn", "this email is used", "Ok");
                }
                else if (returnMsg == "invaildEmail")
                {
                    await DisplayAlert("SingIn", "mail format is incorrect", "Ok");
                }
                else if (returnMsg == "unknow error")
                {
                    //To Do:open ticket
                    await DisplayAlert("SingIn", "unknow error", "Ok");
                }
                else
                {

                    App.Current.MainPage = new SignUp2(returnMsg,pass,mail);
                   
                }

            }
        }
    }
    
}