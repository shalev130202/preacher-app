﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using preacher.Interface;
using preacher.View;
namespace preacher
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage(string email,string pass)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            SizeChanged += MainPage_SizeChanged; 
            InitializeComponent();
            entryEmail.Text = email;
            entryPassword.Text = pass;
            forgotPass.GestureRecognizers.Add(new TapGestureRecognizer((view) => forgotpass()));
            signupLayout.GestureRecognizers.Add(new TapGestureRecognizer((view) => SignUP()));

        }

        private void MainPage_SizeChanged(object sender, EventArgs e)
        {
            if (this.Width > this.Height)
            {
                Xamarin.Forms.Thickness x = new Xamarin.Forms.Thickness(0.0, 10.0, 0.0, 10.0);
                signuplabel.Margin = x;
                x = new Xamarin.Forms.Thickness(0.0, 10.0, 0.0, 0.0);
                signupLayout.Margin = x;
                x = new Xamarin.Forms.Thickness(175.0, 0.0,175.0, 0.0);
                btnLogin.Margin = x;
                btnLogin.HeightRequest = 40;
                btnLogin.FontSize = 12;
                logo.HeightRequest = Width * 0.15;
            }
            else
            {
                Xamarin.Forms.Thickness x = new Xamarin.Forms.Thickness(0.0, 15.0, 0.0, 15.0);
                signuplabel.Margin = x;
                x = new Xamarin.Forms.Thickness(0.0, 15.0, 0.0, 0.0);
                signupLayout.Margin = x;
                x = new Xamarin.Forms.Thickness(10.0, 0.0, 10.0, 0.0);
                btnLogin.Margin = x;
                btnLogin.HeightRequest = 50;
                btnLogin.FontSize = 14;
                
                logo.HeightRequest = Width * 0.4;
            }
            
     
        }

        private void forgotpass()
        {
            App.Current.MainPage = new NavigationPage(new ForgotPassword(entryEmail.Text));
        }
        private void SignUP()
        {
            App.Current.MainPage = new NavigationPage(new SignUp());
        }

        private async void SignIn_Clicked(object sender, EventArgs e)
        {
            string mail = entryEmail.Text;
            string pass = entryPassword.Text;

            if ((mail == null || mail == "" )&& (pass == null || pass == ""))
            {
                await DisplayAlert("SingIn", "please fill out the password and email field ", "Ok");
            }
            else if (pass == null || pass == "")
            {
                await DisplayAlert("SingIn", "please fill out the password field ", "Ok");
            }
            else if (mail == null || mail == "")
            {
                await DisplayAlert("SingIn", "please fill out the email field ", "Ok");
            }
            else
            {
                var fblogin = DependencyService.Get<IFirebase>();

                string returnMsg = await fblogin.DoLoginWithEP(mail, pass); ;

                if (returnMsg == "weakPassword")
                {
                    await DisplayAlert("SingIn", "your password is too weak", "Ok");
                }
                else if (returnMsg == "user_already_exists")
                {
                    await DisplayAlert("SingIn", "this email is used", "Ok");
                }
                else if (returnMsg == "invaildEmail")
                {
                    await DisplayAlert("SingIn", "mail format is incorrect", "Ok");
                }
                else if (returnMsg.Length > 300)
                {
                    await DisplayAlert("SingIn", "login successful!", "Ok");
                    //login complete

                }
                else if (returnMsg == "not_match")
                {
                    await DisplayAlert("SingIn", "the email or password you entered is incorrect", "Ok");
                }
                else
                {

                    await DisplayAlert("SingIn", returnMsg, "Ok");
                }
            }
        }
    }
}
