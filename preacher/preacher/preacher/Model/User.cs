﻿using System;
using System.Collections.Generic;
using System.Text;


namespace preacher.Model
{
    public class User
    {
        public string UserName { get; set; }
        public string Token { get; set; }
        public string ProfileImage { get; set; }
        public string BirthDate { get; set; }
        public string category { get; set; }
        public string Information { get; set; }
        
    }
}
