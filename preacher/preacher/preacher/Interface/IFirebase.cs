﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace preacher.Interface
{
    public interface IFirebase
    {
        Task<string> DoLoginWithEP(string E, string P);
        Task<string> DoRegisterWithEP(string E, string P);
        void DeleteUser(string pass,string email);
        void ResetPassword(string E);
       

    }
}
