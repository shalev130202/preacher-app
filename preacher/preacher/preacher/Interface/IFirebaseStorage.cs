﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace preacher.Interface
{
    public interface IFirebaseStorage
    {
        Task<string> UploadFile(Stream fileStream, string fileName);
        Task<string> GetFile(string fileName);
        Task DeleteFile(string fileName);

    }
}
