﻿using preacher.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace preacher.Interface
{
    public interface IFirebaseDatabase
    {
        Task AddUser(User user);
    }
}
